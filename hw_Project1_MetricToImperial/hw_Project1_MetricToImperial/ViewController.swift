//
//  ViewController.swift
//  hw_Project1_MetricToImperial
//
//  Created by Gilbert on 5/17/19.
//  Copyright © 2019 Hop. All rights reserved.
//

// Task:
// display Welcome a title and a welcome message when opening the app
// two selection button to convert both ways
// Drop down menu that lets to select etweenn: weight, length, volume, Temperature
// after selection is made add a text box to enter values
// another drop down for conversion
// convert button below change to start over button once clicked and results are made
// results label ar the bottom.

/*
Weight: Pounds,Ounces, Stone, Milligrams, Grams, Kilograms, Tons

Length: Inches, Feet, Yards, Miles, Millimeters, Centimeters, Meters, Kilometers

Volume: Teaspoon, Tablespoon, Cups, Quarts, Gallons, Milliliters, Deciliters, Liters
Temperature: Celsius and Fahrenheit
*/




import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var titleLael: UILabel!
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var userInput: UITextField!
    @IBOutlet weak var resultsLabel: UILabel!
    @IBOutlet weak var selectSegment: UISegmentedControl!
    @IBOutlet weak var conversionTypeSegment: UISegmentedControl!
    @IBOutlet weak var conversionSelection: UISegmentedControl!
    @IBOutlet weak var conversionSelection2: UISegmentedControl!
    @IBOutlet weak var convertButton: UIButton!
    
    let iWeight : Array = [ "Ounces", "Pounds", "Stone", "Tons"];
    let mWeight : Array = ["Milligrams", "Grams", "Kilograms"];
    let iLength : Array = ["Inches", "Feet", "Yard", "Miles"];
    let mLength : Array = ["Millimeters", "Centimeters", "Meters", "Kilometers"];
    let iVolume : Array = ["Teaspoon", "Tablespoon", "Cups", "Quarts", "Gallons"];
    let mVolume : Array = ["Milliliters", "Deciliters", "Liters"];
    let temperature : Array = ["Celsius", "Fahrenheit"];

    


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // start with imperial weight
        self.ConversionOptions(self.mWeight, 0);
        self.ConversionOptions(self.iWeight, 1);
        
    }

    @IBAction func convertButton(_ sender: UIButton) {
        calculateConversion();
        print("test")
    }
    
    @IBAction func metricImperialActionn(_ sender: Any) {
        let title : String = self.selectSegment.titleForSegment(at: self.selectSegment.selectedSegmentIndex)!;
        if (self.conversionTypeSegment.selectedSegmentIndex == 0){
            self.ButtonViews(title);
            self.convertButton.setTitle("Convert Metric to Imperial", for: .normal)
        } else {
            self.ButtonViews(title);
            self.convertButton.setTitle("Convert Imperial to Metric", for: .normal)
        }
        self.resetSettings();
    }
    
    
    func resetSettings() {
        self.userInput.text?.removeAll();
        self.resultsLabel.text = "Results:";
    }
    
    
    
    
    @IBAction func TypeConversionAction(_ sender: UISegmentedControl) {
        let title : String = self.selectSegment.titleForSegment(at: self.selectSegment.selectedSegmentIndex)!;
        //  print(title)
        self.ButtonViews(title);
        
    }
    
    func ButtonViews(_ title : String) {
        //do if statement to check metric or imperial
        switch title {
            case "Weight":
                if (self.conversionTypeSegment.selectedSegmentIndex == 0){
                    self.ConversionOptions(self.mWeight, 0);
                    self.ConversionOptions(self.iWeight, 1);
                } else {
                    self.ConversionOptions(self.iWeight, 0);
                    self.ConversionOptions(self.mWeight, 1);
                }
            case "Length":
                if (self.conversionTypeSegment.selectedSegmentIndex == 0){
                    self.ConversionOptions(self.mLength, 0);
                    self.ConversionOptions(self.iLength, 1);
                } else {
                    self.ConversionOptions(self.iLength, 0);
                    self.ConversionOptions(self.mLength, 1);
                }
            case "Volume":
                if (self.conversionTypeSegment.selectedSegmentIndex == 0){
                    self.ConversionOptions(self.mVolume, 0);
                    self.ConversionOptions(self.iVolume, 1);
                } else {
                    self.ConversionOptions(self.iVolume, 0);
                    self.ConversionOptions(self.mVolume, 1);
                }
            case "Temperature":
                self.ConversionOptions(self.temperature, 0);
                self.conversionSelection2.removeAllSegments();
            default:
                print("Selection not found.")
        }
    }
    
    
    
    
    // function to remove and add new segemnt for conversion type 1
    func ConversionOptions(_ Array: Array<String>, _ option : Int?) {
        
        if (option == 0){
            self.conversionSelection.removeAllSegments();
            for num in Array.indices {
                self.conversionSelection.insertSegment(withTitle: Array[num], at: num, animated: true)
            }
            self.conversionSelection.selectedSegmentIndex = 0;
        } else {
            self.conversionSelection2.removeAllSegments();
            for num in Array.indices {
                self.conversionSelection2.insertSegment(withTitle: Array[num], at: num, animated: true)
            }
            self.conversionSelection2.selectedSegmentIndex = 0;
        }
        
        
    }

// temp: To convert Celcius (c) to Fahrenheit, use the formula (c * 1.8) + 32

    func calculateConversion() {
//        print("""
//            First selection: \(String(describing: self.selectSegment.titleForSegment(at: self.selectSegment.selectedSegmentIndex)!))
//            Second Type: \(String(describing: self.conversionTypeSegment.titleForSegment(at: self.conversionTypeSegment.selectedSegmentIndex)!))
//            Thirds selection : \(String(describing: self.conversionSelection.titleForSegment(at: self.conversionSelection.selectedSegmentIndex)!))
//            Fourth Text Value: \(String(describing: self.userInput.text!))
//            Fifth selection: \(String(describing: self.conversionSelection2.titleForSegment(at: self.conversionSelection2.selectedSegmentIndex)!))
//        """)
        
        let title : String = self.selectSegment.titleForSegment(at: self.selectSegment.selectedSegmentIndex)!;
        //  print(title)
        
        if userInput.text != nil {
            //do if statement to check metric or imperial
            switch title {
                case "Weight":
                    // change to selected title first and second
                    self.Weight(String(describing: self.conversionSelection.titleForSegment(at: self.conversionSelection.selectedSegmentIndex)!), String(describing: self.conversionSelection2.titleForSegment(at: self.conversionSelection2.selectedSegmentIndex)!), Float(String(describing: self.userInput.text!)) as! Float)
                case "Length":
                   self.Length(String(describing: self.conversionSelection.titleForSegment(at: self.conversionSelection.selectedSegmentIndex)!), String(describing: self.conversionSelection2.titleForSegment(at: self.conversionSelection2.selectedSegmentIndex)!), Float(String(describing: self.userInput.text!)) as! Float)
                case "Volume":
                    print("Volume")
                    self.Volume(String(describing: self.conversionSelection.titleForSegment(at: self.conversionSelection.selectedSegmentIndex)!), String(describing: self.conversionSelection2.titleForSegment(at: self.conversionSelection2.selectedSegmentIndex)!), Float(String(describing: self.userInput.text!)) as! Float)
                case "Temperature":
                    print("Temperature")
                    self.Temperature(String(describing: self.conversionSelection.titleForSegment(at: self.conversionSelection.selectedSegmentIndex)!), Float(String(describing: self.userInput.text!)) as! Float)
                default:
                    print("Selection not found.")
            }
        }
    }


    //     let temperature : Array = ["Celsius", "Fahrenheit"];
    func Temperature(_ type1 : String, _ value : Float){
            print("inside temperature function")
            var type2 : String = "Fahrenheit";
            if (type1 == "Celsius") {
                type2 = "Fahrenheit";
            } else {
                type2 = "Celsius";
            }
    
            switch type1 {
                case "Celsius":
                    print("Celsius")
                    let results : Float = (value * 1.8000 ) + 32 ;
                        if (results == 0) {return}
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                case "Fahrenheit":
                    print("Fahrenheit")
                    let results : Float = (value - 32) / 1.8000;
                        if (results == 0) {return}
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                default:
                    print("Invalid. 000")
            }
    }





//    let iVolume : Array = ["Teaspoon", "Tablespoon", "Cups", "Quarts", "Gallons"];
//    let mVolume : Array = ["Milliliters", "Deciliters", "Liters"];
    func Volume(_ type1 : String, _ type2 : String, _ value : Float){
        if(self.conversionTypeSegment.selectedSegmentIndex == 0){
            switch type1 {
                case "Milliliters":
                    print("Milliliters")
                    if(type2 == "Teaspoon"){
                        let results : Float = value * 0.20288;
                        if (results == 0) {return}
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Tablespoon") {
                        let results : Float =  value * 0.067628;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Cups") {
                        let results : Float =  value * 0.0042268;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Quarts") {
                        let results : Float =  value * 0.0010567;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    } else {
                    // Gallon
                    let results : Float =  value * 0.00026417;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                case "Deciliters":
                    print("Deciliters")
                    if(type2 == "Teaspoon"){
                        let results : Float = value / 0.0492892159;
                        if (results == 0) {return}
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Tablespoon") {
                        let results : Float =  value / 0.147867648;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Cups") {
                        let results : Float =  value / 2.365882365;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Quarts") {
                        let results : Float =  value / 9.46352946;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    } else {
                    // Gallon
                    let results : Float =  value / 37.85411784;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                case "Liters":
                    print("Liters")
                    if(type2 == "Teaspoon"){
                        let results : Float = value * 202.88;
                        if (results == 0) {return}
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Tablespoon") {
                        let results : Float =  value * 67.628;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Cups") {
                        let results : Float =  value * 4.2268;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Quarts") {
                        let results : Float =  value * 1.0567;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    } else {
                    // Gallon
                    let results : Float =  value * 0.26417;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                default:
                    print("Invalid. 000")
            }
        } else {
            switch type1 {
                case "Teaspoon":
                    print("Teaspoon")
                    if(type2 == "Milliliters"){
                        let results : Float = value / 0.20288;
                        if (results == 0) {return}
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Deciliters") {
                        let results : Float =  value * 0.0492892159;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    } else {
                    // Deciliters
                    let results : Float =  value / 202.88;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                case "Tablespoon":
                    print("Tablespoon")
                    if(type2 == "Milliliters"){
                        let results : Float = value / 0.067628;
                        if (results == 0) {return}
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Deciliters") {
                        let results : Float =  value * 0.147867648;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    } else {
                    // Liters
                    let results : Float =  value / 67.628;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                case "Cups":
                    print("Cups")
                    if(type2 == "Milliliters"){
                        let results : Float = value / 0.0042268;
                        if (results == 0) {return}
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Deciliters") {
                        let results : Float =  value * 2.365882365;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    } else {
                    // Liters
                    let results : Float =  value / 4.2268;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                case "Quarts":
                    print("Tablespoon")
                    if(type2 == "Milliliters"){
                        let results : Float = value * 0.015625;
                        if (results == 0) {return}
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Deciliters") {
                        let results : Float =  value * 9.46352946;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    } else {
                    // Liters
                    let results : Float =  value / 1.0567;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                case "Gallons":
                    print("Gallons")
                    if(type2 == "Milliliters"){
                        let results : Float = value / 0.00026417;
                        if (results == 0) {return}
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Deciliters") {
                        let results : Float =  value * 0.067628;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    } else {
                    // Deciliters
                    let results : Float =  value * 37.85411784;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                default:
                    print("Invalid. 001")
            }
        }
    }









//    let iLength : Array = ["Inches", "Feet", "Yard", "Miles"];
//    let mLength : Array = ["Millimeters", "Centimeters", "Meters", "Kilometers"];

    func Length(_ type1 : String, _ type2 : String, _ value : Float) {
        if(self.conversionTypeSegment.selectedSegmentIndex == 0){
            switch type1 {
                case "Millimeters":
                    print("Millimeters")
                    if(type2 == "Inches"){
                        let results : Float = value * 0.039370;
                        if (results == 0) {return}
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Feet") {
                        print("Centimeters")
                        let results : Float =  value * 0.0032808;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Yard") {
                        let results : Float =  value * 0.0010936;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Miles") {
                        let results : Float =  value * 0.00000062137;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                case "Centimeters":
                    print("Centimeters")
                    if(type2 == "Inches"){
                        let results : Float = value * 0.39370;
                        if (results == 0) {return}
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Feet") {
                        print("Centimeters")
                        let results : Float =  value * 0.032808;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Yard") {
                        let results : Float =  value * 0.010936;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Miles") {
                        let results : Float =  value * 0.0000062137;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                
                case "Meters":
                    print("Meters")
                    if(type2 == "Inches"){
                        let results : Float = value * 39.370;
                        if (results == 0) {return}
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Feet") {
                        print("Centimeters")
                        let results : Float =  value * 3.2808;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Yard") {
                        let results : Float =  value * 1.0936;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Miles") {
                        let results : Float =  value * 0.00062137;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                case "Kilometers":
                    print("Kilometers")
                    if(type2 == "Inches"){
                        let results : Float = value * 39370;
                        if (results == 0) {return}
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Feet") {
                        print("Centimeters")
                        let results : Float =  value * 3280.8;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Yard") {
                        let results : Float =  value * 1093.6;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Miles") {
                        let results : Float =  value * 0.62137;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                default:
                    print("Invalid. 000")
            }
        } else {
                switch type1 {
                case "Inches":
                    print("Inches")
                    if(type2 == "Millimeters"){
                        let results : Float = value / 0.039370;
                        if (results == 0) {return}
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Centimeters") {
                        print("Centimeters")
                        let results : Float =  value / 0.39370;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Meters") {
                        let results : Float =  value / 39.370;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Kilometers") {
                        let results : Float =  value / 39370;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                case "Feet":
                   print("Feet")
                    if(type2 == "Millimeters"){
                        let results : Float = value / 0.0032808;
                        if (results == 0) {return}
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Centimeters") {
                        print("Centimeters")
                        let results : Float =  value / 0.032808;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Meters") {
                        let results : Float =  value / 3.2808;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Kilometers") {
                        let results : Float =  value / 3280.8;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                
                case "Yard":
                    print("Yard")
                    if(type2 == "Millimeters"){
                        let results : Float = value * 36.0;
                        if (results == 0) {return}
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Centimeters") {
                        print("Centimeters")
                        let results : Float =  value / 0.010936;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Meters") {
                        let results : Float =  value / 1.0936;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Kilometers") {
                        let results : Float =  value / 1093.6;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                case "Miles":
                        print("Miles")
                    if(type2 == "Millimeters"){
                        let results : Float = value / 0.00000062137;
                        if (results == 0) {return}
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Centimeters") {
                        print("Centimeters")
                        let results : Float =  value / 0.0000062137;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Meters") {
                        let results : Float =  value / 0.00062137;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Kilometers") {
                        let results : Float =  value / 0.62137;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                default:
                    print("Invalid. 001")
            }
        }
    }



    // let iWeight : Array = [ "Grams", "Ounces", "Pounds", "Tons"];
    // let mWeight : Array = ["Milligrams", "Grams", "Kilograms", "Stone"];
    func Weight(_ type1 : String, _ type2 : String, _ value : Float){
        if(self.conversionTypeSegment.selectedSegmentIndex == 0){
            switch type1 {
                case "Milligrams":
                    print("Milligrams")
                    if(type2 == "Grams"){
                        let results : Float = value/1000;
                        if (results == 0) {return}
                        
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Ounces") {
                        let results : Float =  value * 0.00003527396195;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Pounds") {
                        let results : Float =  value * 0.0000022046;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Stone") {
                        let results : Float =  value * 0.00000015747;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    } else {
                        // type2 == Tons
                        let results : Float =  value * 0.0000000011023;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                case "Grams":
                    print("Grams")
                    if (type2 == "Ounces") {
                        let results : Float =  value * 0.035274;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Pounds") {
                        let results : Float =  value * 0.00220462262;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Stone") {
                        let results : Float =  value * 0.00015747;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    } else {
                        // type2 == Tons
                        let results : Float =  value * 0.0000011023;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                case "Kilograms":
                    print("Kilograms")
                    if (type2 == "Ounces") {
                        let results : Float =  value / 0.0283495231;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Pounds") {
                        let results : Float =  value * 0.45359237;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Stone") {
                        let results : Float =  value / 6.35029318;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    } else {
                        // type2 == Tons
                        let results : Float =  value * 0.001102;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                default:
                    print("Invalid. 000")
            }
        } else {
            switch type1 {
                case "Ounces":
                    print("Ounces")
                    if(type2 == "Milligrams"){
                        let results : Float = value / 0.000035274;
                        if (results == 0) {return}
                        
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Grams") {
                        let results : Float =  value * 28.34952;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Kilograms") {
                        let results : Float =  value * 0.0283495231;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                case "Pounds":
                    print("Pounds")
                    if(type2 == "Milligrams"){
                        let results : Float = value / 0.0000022046;
                        if (results == 0) {return}
                        
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Grams") {
                        let results : Float =  value / 0.0022046;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    } else {
                        print("Kilograms")
                        let results : Float =  value / 0.0022046;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                case "Stone":
                    print("Stone")
                    if(type2 == "Milligrams"){
                        let results : Float = value / 0.00000015747;
                        if (results == 0) {return}
                        
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Grams") {
                        let results : Float =  value / 0.00015747;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    } else {
                        print("Kilograms")
                        let results : Float =  value / 0.15747;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                case "Tons":
                    print("Tons")
                    if(type2 == "Milligrams"){
                        let results : Float = value / 0.0000000011023;
                        if (results == 0) {return}
                        
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                    else if (type2 == "Grams") {
                        let results : Float =  value * 907184.74;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    } else {
                        let results : Float =  value / 0.001102;
                        if (results == 0) {return }
                        self.resultsLabel.text = """
                            Results:
                            \(type1) to \(type2) = \(results)
                        """
                    }
                default:
                    print("Invalid. 001")
            }
        }
    }


    

}

